build-websocket-auth:
	cd lambdas/websocket-auth; zip -r websocket-auth.zip .
	mv lambdas/websocket-auth/websocket-auth.zip terraform/zips/lambdas

build-websocket-connect:
	cd lambdas/websocket-connect; zip -r websocket-connect.zip .
	mv lambdas/websocket-connect/websocket-connect.zip terraform/zips/lambdas

build-websocket-disconnect:
	cd lambdas/websocket-disconnect; zip -r websocket-disconnect.zip .
	mv lambdas/websocket-disconnect/websocket-disconnect.zip terraform/zips/lambdas

build-websocket-receive-message:
	cd lambdas/websocket-receive-message; zip -r websocket-receive-message.zip .
	mv lambdas/websocket-receive-message/websocket-receive-message.zip terraform/zips/lambdas

build-websocket-send-message:
	cd lambdas/websocket-send-message; zip -r websocket-send-message.zip .
	mv lambdas/websocket-send-message/websocket-send-message.zip terraform/zips/lambdas

build-all: build-websocket-auth build-websocket-connect build-websocket-disconnect build-websocket-receive-message build-websocket-send-message

tf-apply:
	cd terraform; terraform apply