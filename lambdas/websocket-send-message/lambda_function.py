import boto3
import json
from os import getenv

API_GATEWAY_ENDPOINT = getenv('API_GATEWAY_ENDPOINT')
api_gateway = boto3.client('apigatewaymanagementapi', endpoint_url=API_GATEWAY_ENDPOINT)

def lambda_handler(event, context):
    connection_id = event.get('connection_id')
    msg = event.get('msg')
    api_gateway.post_to_connection(ConnectionId=connection_id, Data=json.dumps(msg).encode('utf-8'))
    return {
        'statusCode': 200
    }