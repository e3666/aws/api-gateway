def lambda_handler(event, context):
    authorization = event.get('headers', {}).get('Authorization')
    auth_state = 'Deny'
    if authorization == 'Basic 123':
        auth_state = 'Allow'
    
    id_client = 1
    response = {
        "principalId": id_client,
        "policyDocument": {
            "Version": "2012-10-17",
            "Statement": [{
                "Action": "execute-api:Invoke",
                "Effect": auth_state,
                "Resource": event.get('methodArn')
            }]
        }
    }

    return response
