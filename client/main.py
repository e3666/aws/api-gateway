import asyncio
import websockets
import json
from os import getenv
from dotenv import load_dotenv
load_dotenv()

URL = getenv('URL')
extra_headers = {'Authorization': 'Basic 123'}
async def main():
    async with websockets.connect(URL, compression=None, extra_headers=extra_headers) as websocket:
        print('conexão estabelecida....')
        while True:
            recv_msg = await websocket.recv()
            recv_msg = json.loads(recv_msg)
            print(recv_msg)
            msg = {
                'action': "receive_message", 
                'msg': {
                    'text': "msg recebida aqui"
                }
            }
            await websocket.send(json.dumps(msg))
            print('msg enviada....')


asyncio.run(main())