<img src="esquema.svg"/>

Para replicar a infra com o terraform, você deve primeiro instalar o mesmo, para isso siga os passos no site oficial [instalar terraform](https://www.terraform.io/downloads.html).

E agora você vai ter que adicionar dois aquivos dentro da pasta `terraform`, o `main.tf` e `variables.tf`, que vão conter respectivamente informações de conexão com a aws e valores de variáveis (id da conta e região)

- `main.tf`
```ruby
# veja mais sobre providers em https://registry.terraform.io/providers/hashicorp/aws/latest/docs

provider "aws" {
    region  = "região-aws"
    profile = "seu-perfil-configurado-na-cli"
}
```

- `variables.tf`
```ruby
# veja mais sobre variables em https://www.terraform.io/docs/language/values/variables.html

variable "region" {
    default="região-aws"
}

variable "account_id" {
    default="id-da-sua-conta-aws"
}
```

Logo depois de criar esses arquivos, você pode executar os comandos do make file

> OBS.: Talvez seja necessário criar a pasta terraform/zips/lambdas

```shell
$ make build-all
$ make tf-apply
```