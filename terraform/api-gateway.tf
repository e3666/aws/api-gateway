resource "aws_apigatewayv2_api" "api_websocket" {
    name                       = "api-websocket"
    protocol_type              = "WEBSOCKET"
    route_selection_expression = "$request.body.action"
    tags = {
        service = "websocket"
    }
}

resource "aws_apigatewayv2_stage" "prd_stage" {
    api_id      = aws_apigatewayv2_api.api_websocket.id
    name        = "prd"
}