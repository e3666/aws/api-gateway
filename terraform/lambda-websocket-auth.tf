resource "aws_iam_policy" "websocket_auth" {
    name          = "websocket-auth"
    description   = ""
    policy        = jsonencode({
        Version   = "2012-10-17"
        Statement = [
            {
                Effect = "Allow"
                Action = [
                    "logs:CreateLogGroup"
                ]
                Resource = "arn:aws:logs:${var.region}:${var.account_id}:*"
            },
            {
                Effect = "Allow"
                Action = [
                    "logs:CreateLogStream",
                    "logs:PutLogEvents"
                ]
                Resource = "arn:aws:logs:${var.region}:${var.account_id}:log-group:/aws/lambda/websocket-auth:*"
            }
        ]
    })
}


resource "aws_iam_role" "websocket_auth" {
    name               = "websocket-auth"
    assume_role_policy = jsonencode({
        Version   = "2012-10-17"
        Statement = [
            {
                Sid       = ""
                Effect    = "Allow"
                Action    = ["sts:AssumeRole"]
                Principal = {
                    Service = "lambda.amazonaws.com"
                }
            }
        ]
    })
    managed_policy_arns = [aws_iam_policy.websocket_auth.arn]
}

resource "aws_lambda_function" "websocket_auth" {
    function_name    = "websocket-auth"
    handler          = "lambda_function.lambda_handler"
    runtime          = "python3.9"
    memory_size      = 128
    timeout          = 3
    role             = aws_iam_role.websocket_auth.arn
    filename         = "zips/lambdas/websocket-auth.zip"
    source_code_hash = filebase64sha256("zips/lambdas/websocket-auth.zip")
    layers           = []
    tags             = {
        service = "websocket"
    }
}
