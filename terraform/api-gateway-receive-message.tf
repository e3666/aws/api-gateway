resource "aws_apigatewayv2_integration" "receive_message" {
    api_id                 = aws_apigatewayv2_api.api_websocket.id
    integration_uri        = aws_lambda_function.websocket_receive_message.invoke_arn
    integration_type       = "AWS_PROXY"
    integration_method     = "POST"
    passthrough_behavior   = "WHEN_NO_MATCH"
}

resource "aws_apigatewayv2_route" "receive_message" {
    api_id    = aws_apigatewayv2_api.api_websocket.id
    route_key = "receive_message"
    target    = "integrations/${aws_apigatewayv2_integration.receive_message.id}"
}

resource "aws_lambda_permission" "receive_message" {
    statement_id  = "AllowExecutionFromAPIGateway"
    action        = "lambda:InvokeFunction"
    function_name = aws_lambda_function.websocket_receive_message.function_name
    principal     = "apigateway.amazonaws.com"
    source_arn    = "${aws_apigatewayv2_api.api_websocket.execution_arn}/*/*"
}