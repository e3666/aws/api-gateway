resource "aws_iam_policy" "websocket_disconnect" {
    name          = "websocket-disconnect"
    description   = ""
    policy        = jsonencode({
        Version   = "2012-10-17"
        Statement = [
            {
                Effect = "Allow"
                Action = [
                    "logs:CreateLogGroup"
                ]
                Resource = "arn:aws:logs:${var.region}:${var.account_id}:*"
            },
            {
                Effect = "Allow"
                Action = [
                    "logs:CreateLogStream",
                    "logs:PutLogEvents"
                ]
                Resource = "arn:aws:logs:${var.region}:${var.account_id}:log-group:/aws/lambda/websocket-disconnect:*"
            }
        ]
    })
}

resource "aws_iam_role" "websocket_disconnect" {
    name               = "websocket-disconnect"
    assume_role_policy = jsonencode({
        Version   = "2012-10-17"
        Statement = [
            {
                Sid       = ""
                Effect    = "Allow"
                Action    = ["sts:AssumeRole"]
                Principal = {
                    Service = "lambda.amazonaws.com"
                }
            }
        ]
    })
    managed_policy_arns = [aws_iam_policy.websocket_disconnect.arn]
}

resource "aws_lambda_function" "websocket_disconnect" {
    function_name    = "websocket-disconnect"
    filename         = "zips/lambdas/websocket-disconnect.zip"
    runtime          = "python3.9"
    memory_size      = 128
    timeout          = 3
    role             = aws_iam_role.websocket_disconnect.arn
    handler          = "lambda_function.lambda_handler"
    source_code_hash = filebase64sha256("zips/lambdas/websocket-disconnect.zip")
    layers           = []
    tags             = {
        service = "websocket"
    }
}
