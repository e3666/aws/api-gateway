resource "aws_apigatewayv2_authorizer" "connect" {
    api_id           = aws_apigatewayv2_api.api_websocket.id
    authorizer_type  = "REQUEST"
    authorizer_uri   = aws_lambda_function.websocket_auth.invoke_arn
    identity_sources = ["route.request.header.Authorization"]
    name             = "lambda-auth"
}

resource "aws_apigatewayv2_integration" "connect" {
    api_id                 = aws_apigatewayv2_api.api_websocket.id
    integration_uri        = aws_lambda_function.websocket_connect.invoke_arn
    integration_type       = "AWS_PROXY"
    integration_method     = "POST"
    passthrough_behavior   = "WHEN_NO_MATCH"
}

resource "aws_apigatewayv2_route" "connect" {
    api_id             = aws_apigatewayv2_api.api_websocket.id
    route_key          = "$connect"
    target             = "integrations/${aws_apigatewayv2_integration.connect.id}"
    authorization_type = "CUSTOM"
    authorizer_id      = aws_apigatewayv2_authorizer.connect.id
}

resource "aws_lambda_permission" "connect" {
    statement_id  = "AllowExecutionFromAPIGateway"
    action        = "lambda:InvokeFunction"
    function_name = aws_lambda_function.websocket_connect.function_name
    principal     = "apigateway.amazonaws.com"
    source_arn    = "${aws_apigatewayv2_api.api_websocket.execution_arn}/*/*"
}

resource "aws_lambda_permission" "connect_authorizer" {
    statement_id  = "AllowExecutionFromAPIGateway"
    action        = "lambda:InvokeFunction"
    function_name = aws_lambda_function.websocket_auth.function_name
    principal     = "apigateway.amazonaws.com"
    source_arn    = "${aws_apigatewayv2_api.api_websocket.execution_arn}/*/*"
}